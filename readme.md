Sonata retail review
=======================

Hello, fellow TapTap developer!

If you are reading this you probably have thought about reading my code ... So good luck with it! It sure needs more improvements, you can mail me at rafinskipg@gmail.com for ideas about cleaning things.


API
==========

```
{
  "salute": "What's up? This is the todo list",
  "methods": [
    {
      "name": "/",
      "description": "Returns available methods"
    },
    {
      "name": "/user",
      "description": "Returns users list"
    },
    {
      "name": "/user:id",
      "description": "Returns user information"
    },
    {
      "name": "/api/user/:id",
      "description": "Returns the tasks for a given user"
    },
    {
      "name": "/api/task",
      "description": "GET all tasks, POST task"
    },
    {
      "name": "/api/task/:id",
      "description": "PUT task, DELETE task"
    }
  ]
}

```

TODO
===========

I haven't implemented session management.
